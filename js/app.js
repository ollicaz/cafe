const cartBtns = document.querySelectorAll('.coffe-box__btn')
const cartWrap = document.querySelector('.wrap')
const closePopup = document.querySelector('.close')

Array.from(cartBtns).forEach(cartBtn => {
    cartBtn.addEventListener('click', () => {
        cartWrap.style.display = 'block'
    });
});

closePopup.addEventListener('click', () => {
    closePopup.style.display = 'none'
});
